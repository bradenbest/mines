#include <stdio.h>
// This is a test. I want it to print "g", without having to hit enter. Just G.
int main(){
    char c;
    while(1){
        printf("Enter a char: ");
        system("/bin/stty raw");
        c = fgetc(stdin);
        system("/bin/stty cooked");
        if (c == 'q') break;
        else printf("\nChar: %c\n", c); 
    }
    return 0;
}
