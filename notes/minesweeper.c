#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TERMINAL_UNKNOWN  0
#define TERMINAL_UNIX     1
#define TERMINAL_MAC      2
#define TERMINAL_WINDOWS  3
#define TERMINAL TERMINAL_UNIX

#define COLOR(style, bg, fg) \
    "\e[" style ";4" bg ";3" fg "m"

#define COLOR_BG_BLACK   COLOR("1", "0", "7")
#define COLOR_BG_RED     COLOR("1", "1", "7")
#define COLOR_BG_GREEN   COLOR("0", "2", "0")
#define COLOR_BG_YELLOW  COLOR("0", "3", "0")
#define COLOR_BG_BLUE    COLOR("0", "4", "0")
#define COLOR_BG_PURPLE  COLOR("0", "5", "0")
#define COLOR_BG_CYAN    COLOR("0", "6", "0")
#define COLOR_BG_WHITE   COLOR("0", "7", "0")

#define COLOR_BG_BORDER  COLOR_BG_BLUE
#define COLOR_CLEAR_SCREEN "\e[2J"
#define COLOR_RESET        "\e[0;0;37m"

#define DIR_UP         0
#define DIR_UP_RIGHT   1
#define DIR_RIGHT      2
#define DIR_DOWN_RIGHT 3
#define DIR_DOWN       4
#define DIR_DOWN_LEFT  5
#define DIR_LEFT       6
#define DIR_UP_LEFT    7
#define DIR_INVALID    -1

#define INVALID_DIR_UP    (out < 0)
#define INVALID_DIR_RIGHT (out % g->width == 0)
#define INVALID_DIR_DOWN  (out >= (g->width * g->height))
#define INVALID_DIR_LEFT  ( (out % g->width == g->width - 1) || (out < 0) )

#define CHAR_BLANK         ' '
#define CHAR_BOMB          '*'
#define CHAR_BORDER_H      '+'
#define CHAR_BORDER_V      '+'
#define CHAR_FLAG          '!'
#define CHAR_SEL           '@'
#define CHAR_UNKNOWN       '.'
#define CHAR_FLAGGED_BOMB  '#'

#define CHAR_NUMBER(chr) \
    ('0' + chr)

#define MASK_BOMB (1<<0)
#define MASK_FLAG (1<<1)
#define MASK_FLIP (1<<2)

#define DIFFICULTY_S_EASY     16,     16,         5,     3, 0
#define DIFFICULTY_EASY        8,      8,        10,     1, 1
#define DIFFICULTY_MEDIUM     16,     16,        40,     1, 2
#define DIFFICULTY_HARD       30,     16,        99,     1, 3
#define DIFFICULTY_CUSTOM  width, height, num_bombs, lives, 4

#define COMMAND_NULL       0
#define COMMAND_MOVE_UP    1
#define COMMAND_MOVE_RIGHT 2
#define COMMAND_MOVE_DOWN  3
#define COMMAND_MOVE_LEFT  4
#define COMMAND_FLIP       5
#define COMMAND_FLAG       6
#define COMMAND_QUIT       7

#define BLOCK(type, size) \
    (type *)malloc(sizeof(type) * size)

typedef struct game game;
void check_victory( game *g, int id );
void color_print( char c );
int count_adjacent( game *g, int id, int mask );
void draw_border_bottom(game *g);
void draw_border_horiz(int width, char c);
void draw_border_top(game *g);
void flag_square( game *g, int id );
void flip_all(game *g);
void flip_square( game *g, int id );
void game_loop( game *g );
int get_adjacent( game *g, int id, int direction );
int get_key();
char *get_difficulty( game *g );
int get_input( char *prompt );
int get_square( game *g, int x, int y );
int get_x_coord( game *g, int p );
int get_y_coord( game *g, int p );
void move_cursor( game *g, int direction );
game *new_game( int width, int height, int number_of_bombs, int lives, int difficulty );
void parse_command( game *g, int command );
void print_game_info( game *g );
void print_usage();
void quit_game(game *g, int exit_code);
int random_int( int cap );
void render( game *g );

struct game{
    int width;
    int height;
    int num_bombs;
    int num_flipped;
    int num_flagged;
    int lives;
    int deaths;
    int sel_square;
    int difficulty;
    int *board;
};

void check_victory(game *g, int id){/*{{{*/
    int status;
    enum{
        none,
        victory,
        failure
    };
    status = none;
    if (g->board[id] & MASK_BOMB)
        g->deaths ++;
        if(g->deaths == g->lives)
            status = failure;
    if (g->num_flipped == (g->width * g->height) - g->num_bombs)
        status = victory;

    if(status != none){
        flip_all(g);
        render(g);
        print_game_info(g);
        printf("\n\n\n\n\n"); // I don't like doing this, but I don't expect to be messing around with the controls, so...
        printf("You ");
        if (status == victory)
            printf("won!\n");
        else if (status == failure)
            printf("lost.\n");
        quit_game(g, 0);
    }
}/*}}}*/
void color_print(char c){/*{{{*/
    // draw.c
    switch(c){
        case '1':
            printf(COLOR_BG_BLACK "%c " COLOR_RESET, c);
            break;
        case '2':
            printf(COLOR_BG_BLUE "%c " COLOR_RESET, c);
            break;
        case '3':
            printf(COLOR_BG_YELLOW "%c " COLOR_RESET, c);
            break;
        case '4':
            printf(COLOR_BG_GREEN "%c " COLOR_RESET, c);
            break;
        case '5':
            printf(COLOR_BG_PURPLE "%c " COLOR_RESET, c);
            break;
        case '6':
            printf(COLOR_BG_RED "%c " COLOR_RESET, c);
            break;
        case '7':
            printf(COLOR_BG_CYAN "%c " COLOR_RESET, c);
            break;
        case '8':
            printf(COLOR_BG_WHITE "%c " COLOR_RESET, c);
            break;
        default:
            printf("%c ", c);
            break;
    }
}/*}}}*/
int count_adjacent(game *g, int id, int mask){/*{{{*/
    int direction = 0,
        subsel,
        counter = 0;
    while(direction < 8){
        subsel = get_adjacent(g, id, direction);
        if (subsel != DIR_INVALID && g->board[subsel] & mask){
            counter++;
        }
        direction++;
    }
    return counter;
}/*}}}*/
void draw_border_bottom(game *g){/*{{{*/
    //draw.c
    if (TERMINAL == TERMINAL_UNIX){
        // Unix-specific. Draws simple white boxes
        printf(COLOR_BG_BORDER);
        draw_border_horiz(g->width + 2, ' ');
        printf(COLOR_RESET "\n");
    } else {
        printf("%c ", CHAR_BORDER_H);
        draw_border_horiz(g->width + 2, ' ');
        printf("%c\n", CHAR_BORDER_H);

        draw_border_horiz(g->width + 4, CHAR_BORDER_H);
        printf("\n");
    }
}/*}}}*/
void draw_border_horiz(int width, char c){/*{{{*/
    //draw.c
    int i = 0;
    while(i++ < width)
        printf("%c ", c);
}/*}}}*/
void draw_border_top(game *g){/*{{{*/
    //draw.c
    if (TERMINAL == TERMINAL_UNIX){
        // Unix-specific. Same as draw_border_bottom()
        draw_border_bottom(g);
    } else {
        draw_border_horiz(g->width + 4, CHAR_BORDER_H);
        printf("\n");

        printf("%c ", CHAR_BORDER_H);
        draw_border_horiz(g->width + 2, ' ');
        printf("%c\n", CHAR_BORDER_H);
    }
}/*}}}*/
void flag_square(game *g, int id){/*{{{*/
    if ( !(g->board[id] & MASK_FLIP) || ((g->board[id] & MASK_FLIP) && (g->board[id] & MASK_BOMB)) ) {
        // if not flipped, OR a flipped bomb (multiple lives allow this scenario)
        if(g->board[id] & MASK_FLAG) {
            g->num_flagged --;
        } else {
            g->num_flagged ++;
        }
        g->board[id] ^= MASK_FLAG;
    }
}/*}}}*/
void flip_all(game *g){/*{{{*/
    int i = 0;
    while(i < g->width * g->height){
        g->board[i] |= MASK_FLIP;
        i ++;
    }
}/*}}}*/
void flip_square(game *g, int id){/*{{{*/
    int num_bombs,
        num_flags,
        sel,
        direction = 0;
    if (!(g->board[id] & MASK_FLIP) && !(g->board[id] & MASK_FLAG)){
        /* If  square isn't flipped (prevent infinite recursion)
         * and square isn't flagged (prevent flipping a flagged bomb)
         * This section recursively flips non-number squares
         */
        g->board[id] |= MASK_FLIP;
        g->num_flipped ++;
        check_victory(g, id);
        num_bombs = count_adjacent(g, id, MASK_BOMB);
        if (num_bombs == 0){
            while(direction <= 7){
                sel = get_adjacent(g, id, direction);
                if (sel != DIR_INVALID){
                    flip_square(g, sel);
                }
                direction ++;
            }
        }
    } else if (g->board[id] & MASK_FLIP) {
        /* If square IS flipped
         * This special case flips the squares around a numbered square
         * This is a double-edged sword. If you flagged the wrong square, the
         * bomb WILL blow up and end your game
         */
        num_bombs = count_adjacent(g, id, MASK_BOMB);
        num_flags = count_adjacent(g, id, MASK_FLAG);
        if (num_flags == num_bombs && num_bombs > 0){
            while(direction <= 7){
                sel = get_adjacent(g, id, direction);
                if (sel != DIR_INVALID && !(g->board[sel] & MASK_FLIP)){
                    //If valid direction AND square isn't already flipped (again, infinite recursion)
                    flip_square(g, sel);
                }
                direction ++;
            }
        }
    }
}/*}}}*/
void game_loop(game *g){/*{{{*/
    // input.c
    int command;
    while (1){
        render(g);
        print_game_info(g);
        command = get_key();
        parse_command(g, command);
    }
}/*}}}*/
int get_adjacent(game *g, int id, int direction){/*{{{*/
    /* Returns ID of adjacent square in given direction
     * If direction is invalid for given square, returns -1
     */
    int out;
    switch(direction){
        case DIR_UP:
            out = id - g->width;
            if (INVALID_DIR_UP) out = DIR_INVALID;
            break;
        case DIR_RIGHT:
            out = id + 1;
            if (INVALID_DIR_RIGHT) out = DIR_INVALID;
            break;
        case DIR_DOWN:
            out = id + g->width;
            if (INVALID_DIR_DOWN) out = DIR_INVALID;
            break;
        case DIR_LEFT:
            out = id - 1;
            if (INVALID_DIR_LEFT) out = DIR_INVALID;
            break;
            /*
             * Conveniently enough, the diagonal logic is literally just
             * mixing the existing four-direction logic together.
             */
        case DIR_UP_RIGHT:
            out = (id - g->width) + 1;
            if (INVALID_DIR_UP || INVALID_DIR_RIGHT) out = DIR_INVALID;
            break;
        case DIR_DOWN_RIGHT:
            out = (id + g->width) + 1;
            if (INVALID_DIR_DOWN || INVALID_DIR_RIGHT) out = DIR_INVALID;
            break;
        case DIR_DOWN_LEFT:
            out = (id + g->width) - 1;
            if (INVALID_DIR_DOWN || INVALID_DIR_LEFT) out = DIR_INVALID;
            break;
        case DIR_UP_LEFT:
            out = (id - g->width) - 1;
            if (INVALID_DIR_UP || INVALID_DIR_LEFT) out = DIR_INVALID;
            break;
    }
    return out;
}/*}}}*/
int get_key(){/*{{{*/
    //input.c
    char cmd;
    printf("Controls:\n");
    printf("  h/j/k/l - move left/down/up/right\n");
    printf("  w/a/s/d - alternate movement controls\n");
    printf("  f/o     - flip selected square\n");
    printf("  g/p     - fla[g] selected square\n");
    printf("  q       - quit the game\n");

    if (TERMINAL == TERMINAL_UNIX){
        // Unix-specific. Enables input without pressing enter.
        system("/bin/stty raw");
        cmd = getchar();
        system("/bin/stty cooked");
    } else {
        cmd = getchar();
    }
    switch(cmd){
        case 'h':
        case 'a':
            return COMMAND_MOVE_LEFT;
        case 'j':
        case 's':
            return COMMAND_MOVE_DOWN;
        case 'k':
        case 'w':
            return COMMAND_MOVE_UP;
        case 'l':
        case 'd':
            return COMMAND_MOVE_RIGHT;
        case 'f':
        case 'o':
            return COMMAND_FLIP;
        case 'g':
        case 'p':
            return COMMAND_FLAG;
        case 'q':
            return COMMAND_QUIT;
        default:
            return COMMAND_NULL;
    }
}/*}}}*/
char *get_difficulty(game *g){/*{{{*/
    enum{
        s_easy = 0,
        easy   = 1,
        medium = 2,
        hard   = 3,
        custom = 4
    };
    switch(g->difficulty){
        case s_easy:
            return "Super Easy";
        case easy:
            return "Easy";
        case medium:
            return "Medium";
        case hard:
            return "Hard";
        case custom:
            return "Custom";
    }
}/*}}}*/
int get_input(char *prompt){/*{{{*/
    // input.c
    char in[6];
    printf("%s", prompt);
    fgets(in, 6, stdin);
    switch(in[0]){
        case 'h':
            return DIR_LEFT;
        case 'j':
            return DIR_DOWN;
        case 'k':
            return DIR_UP;
        case 'l':
            return DIR_RIGHT;
    }
    return atoi(in);
}/*}}}*/
int get_square(game *g, int x, int y){/*{{{*/
    /* Returns ID of square at (x,y) */
    return x + (y * g->width);
}/*}}}*/
int get_x_coord(game *g, int p){/*{{{*/
    while(p > g->width)
        p -= g->width;
    return p % g->width;
}/*}}}*/
int get_y_coord(game *g, int p){/*{{{*/
    while(p % g->width)
        p --;
    return p / g->width;
}/*}}}*/
void move_cursor(game *g, int direction){ // input.c/*{{{*/
    int out;
    switch(direction){
        case DIR_UP:
            out = g->sel_square - g->width;
            if (!INVALID_DIR_UP)
                g->sel_square = out;
            break;
        case DIR_RIGHT:
            out = g->sel_square + 1;
            if (!INVALID_DIR_RIGHT)
                g->sel_square = out;
            break;
        case DIR_DOWN:
            out = g->sel_square + g->width;
            if (!INVALID_DIR_DOWN)
                g->sel_square = out;
            break;
        case DIR_LEFT:
            out = g->sel_square - 1;
            if (!INVALID_DIR_LEFT)
                g->sel_square = out;
            break;
    }
}/*}}}*/
game *new_game(int width, int height, int number_of_bombs, int lives, int difficulty){/*{{{*/
    game *g  = BLOCK(game, 1);
    int *board = BLOCK(int, width * height);
    int i = 0,
        sel,
        num_bombs_temp = number_of_bombs;
    if (!g || !board){
        printf("Uh oh!\n");
        exit(1);
    }
    while(num_bombs_temp){
        sel = random_int(width * height); // select random square from board
        if(board[sel] & MASK_BOMB)
            continue;

        board[sel] |= MASK_BOMB;
        num_bombs_temp --;
    }

    g->width = width;
    g->height = height;
    g->num_bombs = number_of_bombs;
    g->num_flipped = 0;
    g->num_flagged = 0;
    g->lives = lives;
    g->deaths = 0;
    g->sel_square = 0;
    g->difficulty = difficulty;
    g->board = board;

    return g;
}/*}}}*/
void parse_command(game *g, int command){/*{{{*/
    //input.c
    int num_bombs;
    switch(command){
        case COMMAND_NULL:
            break;
        case COMMAND_MOVE_UP:
            move_cursor(g, DIR_UP);
            break;
        case COMMAND_MOVE_RIGHT:
            move_cursor(g, DIR_RIGHT);
            break;
        case COMMAND_MOVE_DOWN:
            move_cursor(g, DIR_DOWN);
            break;
        case COMMAND_MOVE_LEFT:
            move_cursor(g, DIR_LEFT);
            break;
        case COMMAND_FLIP:
            flip_square(g, g->sel_square);
            break;
        case COMMAND_FLAG:
            flag_square(g, g->sel_square);
            break;
        case COMMAND_QUIT:
            quit_game(g, 0);
            break;
    }
}/*}}}*/
void print_game_info(game *g){/*{{{*/
    //draw.c
    int num_non_bombs;
    float percent[2];
    num_non_bombs = g->width * g->height - g->num_bombs;
    percent[0] = (float)g->num_flipped / (float)num_non_bombs;
    percent[1] = (float)g->num_flagged / (float)g->num_bombs;
    printf("Difficulty: %s (%ix%i, %i bombs)\n", get_difficulty(g), g->width, g->height, g->num_bombs);
    printf("Progress:\n");
    printf("  %i/%i squares flipped (%i%%)\n", g->num_flipped, num_non_bombs, (int)(percent[0] * 100) );
    printf("  %i/%i flags used (%i%%)\n",      g->num_flagged, g->num_bombs,  (int)(percent[1] * 100) );
    printf("  %i/%i lives remain\n",           g->lives - g->deaths, g->lives);
    printf("Position:   %i/%i (%i, %i)\n", g->sel_square, g->width * g->height - 1, get_x_coord(g, g->sel_square), get_y_coord(g, g->sel_square));
}/*}}}*/
void print_usage(){/*{{{*/
    printf("Usage: minesweeper [difficulty]\n");
    printf("Difficulties:\n");
    printf("    --s-easy   -- 16x16,  5 bombs\n");
    printf("    --easy     -- 9x9,   10 bombs\n");
    printf("    --medium   -- 16x16, 40 bombs\n");
    printf("    --hard     -- 30x16, 99 bombs\n");
    printf("    --custom\n");
    exit(1);
}/*}}}*/
void quit_game(game *g, int exit_code){/*{{{*/
    free(g->board);
    free(g);
    exit(exit_code);
}/*}}}*/
int random_int(int cap){/*{{{*/
    return rand() % cap;
}/*}}}*/
void render(game *g){/*{{{*/
    // draw.c
    int i = 0,
        j = 0,
        id;
    char c;
    if (TERMINAL == TERMINAL_UNIX){
        // Unix-specific. Slightly faster and more precise screen clearing
        printf(COLOR_CLEAR_SCREEN "\n");
    } else {
        while(i++ < 100){
            printf("\n");
        }
    }
    i = 0;
    draw_border_top(g);
    while(i < g->height){
        if (TERMINAL == TERMINAL_UNIX){
            // Unix-specific. Color. Left-side border
            printf(COLOR_BG_BORDER "  " COLOR_RESET);
        } else {
            printf("%c   ", CHAR_BORDER_V);
        }
        while(j < g->width){
            id = get_square(g, j, i);
            if ( !(g->board[id] & MASK_FLIP) ){
                c = (g->board[id] & MASK_FLAG) ? CHAR_FLAG : CHAR_UNKNOWN;
            } else {
                if (g->board[id] & MASK_BOMB){
                    if (g->board[id] & MASK_FLAG){
                        c = CHAR_FLAGGED_BOMB;
                    } else {
                        c = CHAR_BOMB;
                    }
                } else {
                    c = (char)count_adjacent(g, id, MASK_BOMB);
                    c = c ? CHAR_NUMBER(c) : CHAR_BLANK;
                }
            }
            if ( id == g->sel_square ){
                if (TERMINAL == TERMINAL_UNIX){
                    // Unix-specific. Prints the character with colors inverted.
                    printf(COLOR_BG_WHITE "%c " COLOR_RESET, c);
                } else {
                    printf("%c%c", CHAR_SEL, c);
                }
            } else {
                if (TERMINAL == TERMINAL_UNIX){
                    // Unix-specific. Prints the numbers with colors
                    color_print(c);
                } else {
                    printf("%c ", c);
                }
            }
            j++;
        }
        if (TERMINAL == TERMINAL_UNIX){
            // Unix-specific. Color. Right-side border
            printf(COLOR_BG_BORDER "  " COLOR_RESET "\n");
        } else {
            printf("  %c\n", CHAR_BORDER_V);
        }
        j = 0;
        i++;
    }
    draw_border_bottom(g);
}/*}}}*/

int main(int argc, char **argv){
    srand(time(0));
    int width,
        height,
        num_bombs,
        lives;
    game *g;
    if(argc > 1) {
        if (!strcmp(argv[1], "--s-easy")){
            g = new_game(DIFFICULTY_S_EASY);
        } else if (!strcmp(argv[1], "--easy")) {
            g = new_game(DIFFICULTY_EASY);
        } else if (!strcmp(argv[1], "--medium")) {
            g = new_game(DIFFICULTY_MEDIUM);
        } else if (!strcmp(argv[1], "--hard")) {
            g = new_game(DIFFICULTY_HARD);
        } else if (!strcmp(argv[1], "--custom")) {
            width     = get_input("Width: ");
            height    = get_input("Height: ");
            num_bombs = get_input("# of bombs: ");
            lives     = get_input("# of lives: ");
            g = new_game(DIFFICULTY_CUSTOM);
        } else {
            printf("Invalid option '%s'.\n", argv[1]);
            print_usage();
        }
    } else {
        print_usage();
    }
    game_loop(g);
    quit_game(g, 0);
    return 0;
}
