Legend:
    *  bomb
    N  N bombs surround this block
       0 bombs surround this block
    ?  Unknown block
    !  flagged block
    .  Selected block

[ ][ ][ ][ ]
[ ][ ][ ][ ]
[ ][ ][ ][ ]
[ ][ ][ ][ ]


Bombs first

[ 0,0,0,1,1,0,0,0,1,1,1,1,0,0,0,0 ]

[ ][ ][ ][*]
[*][ ][ ][ ]
[*][*][*][*]
[ ][ ][ ][ ]

Then figure numbers. 0 = blank


[1][1][1][*]
[*][4][4][3]
[*][*][*][*]
[2][3][3][2]

If click blank (0), then recursively flip all surrounding blocks

Consider this field:

[.][1][1][1]
[ ][1][*][1]
[ ][1][1][1]
[ ][ ][ ][ ]

Flipping where the [.] is, would flip all the blocks surrounding it, which would trigger the block below it to flip all its adjacent blocks, etc. Till it looks like this:

Before:

[?][?][?][?]
[?][?][?][?]
[?][?][?][?]
[?][?][?][?]

After:

[ ][1][?][?]
[ ][1][?][?]
[ ][1][1][1]
[ ][ ][ ][ ]

== 2D Logic ==

0  1  2  3 
4  5  6  7 
8  9  10 11
12 13 14 15

array:  0 - 15
width:  4
height: 4
length: 16

N_up:       N - width            [ 5 - 4 = 1 ]
  Invalid:  result < 0           [ 3 - 4 < 0 ]

N_down:     N + width            [ 5 + 4 = 9 ]
  Invalid:  result >= length     [ 12 + 4 >= 16 ]

N_right:    N + 1                [ 5 + 1 = 6 ]
  Invalid:  result % width == 0  [ 3+1, 7+1, 11+1, 15+1 = 4,8,12,16, all divisible by 4 (n%4 == 0) ] 

N_left:     N - 1
  Invalid:  result % width == width - 1 || result < 0 
    This one was slightly more difficult
    4-1=3, 3%4=3 (invalid)
    5-1=4, 4%4=0 (valid)
    6-1=5, 5%4=1 (valid) 
    7-1=6, 6%4=2 (valid)
    (result%width == width-1)?
        8-1  7  % 4  3 
        9-1  8  % 4  0
        10-1 9  % 4  1
        11-1 10 % 4  2
        (yup, what about 0-1, can -1%4 be something other than 3?)
        (don't want to risk it)

combined direction: (d1 invalid || d2 invalid), if one or the other is invalid, then it's impossible for the combined direction to be valid, since, logically, both directions have to be available for the interim to exist

I'll use a function 
    int get_adjacent(int selected_id, int direction)

0 = up
1 = up-right
2 = right
3 = down-right
4 = down
5 = down-left
6 = left
7 = up-left

The function will return the id of the requested direction, or -1 if the direction is found to be invalid (such is the case when near a wall or corner)

Since no such array index -1 is possible in C, -1 will be interpreted as "that square doesn't exist; ignore it"


== Arrays needed ==

bombs[] (1 or 0)
flags[] (1 or 0)
board[] (char array used to render board)

Numbers/blanks are derived FROM the bombs. 
  for i in {0..8}
    If [adjacent square is valid] and [is a bomb], increment adjacent_bombs
    adjacent_bombs = number displayed. If 0, then blank and recursive flip when clicked

Or...maybe I could just use a singular array and use bitmasks to change the properties

Something like

#define BOMB 1
#define FLAG 2
#define FLIP 4

To add a mask,

    game->board[selected] |= <mask>

To toggle a mask

    game->board[selected] ^= <mask>

To check for a mask

    game->board[selected] & <mask>

It would consume a lot less memory, and bitwise operations are damned fast

== In practise ==

Testing the rendering engine, I made it render this:

1 2 1 1
* 3 * 1
* 5 2 2
* 3 * 1  

Perfecto!

Just made it to put an exact number of bombs in random places

1 1 1 _ 1 1 3 *
1 * 2 1 3 * 5 *
1 1 2 * 3 * 4 *
_ _ 1 1 3 2 3 1
_ _ _ _ 1 * 1 _
1 1 1 1 2 1 1 _
* 1 1 * 1 _ _ _
1 1 1 1 1 _ _ _   

Even better!

The last one was just going through each sqaure and deciding on a 30% chance (random_int(100) < 30) whether to put a bomb there or not.

This one will, ten times, choose a unique, non-bomb square at random, and add a bomb there.

1 1 1 _ 1 1 3 *
1 * 2 1 3 * 5 *
1 1 2 * 3 * 4 *
_ _ 1 1 3 2 3 1
_ _ _ _ 1 * 1 _
1 1 1 1 2 1 1 _
* 1 1 * 1 _ _ _
1 1 1 1 1 _ _ _  <-- flipping this square would flip all the other "_" squares.

Before:

? ? ? ? ? ? ? ?
? ? ? ? ? ? ? ?
? ? ? ? ? ? ? ?
? ? ? ? ? ? ? ?
? ? ? ? ? ? ? ?
? ? ? ? ? ? ? ?
? ? ? ? ? ? ? ?
? ? ? ? ? ? ? ? 

After:

? ? ? ? ? ? ? ?
? ? ? ? ? ? ? ?
? ? ? ? ? ? ? ?
? ? ? ? ? ? 3 1
? ? ? ? ? ? 1 _
? ? ? ? 2 1 1 _
? ? ? ? 1 _ _ _
? ? ? ? 1 _ _ _

Just for emphasis:

. . . . . . . .
. . . . . . . .
. . . . . . . .
. . . . . . 3 1
. . . . . . 1 _
. . . . 2 1 1 _
. . . . 1 _ _ _
. . . . 1 _ _ _

After this, I'm changing "?" to "." and "_" to " ". ?'s and _'s are kinda hard to read. Bad contrast.

Here's a render of a medium difficulty map (32x32, 40 bombs) with all squares flipped:

1 * 1               1 * 1   1 1 1   1 * 2 1     1 1 2 1 1       
2 2 2 1 1 1         1 1 1   1 * 1   1 2 * 1     1 * 3 * 2 1 1   
1 * 1 1 * 1 1 1 1           1 1 1     2 2 2     1 2 * 2 2 * 1   
1 1 1 1 1 1 1 * 1                     1 * 1       1 1 1 1 1 1   
            1 1 1                     1 1 1                     
                1 1 1                                           
1 1 1           1 * 2 1 1             1 1 1   1 1 1             
1 * 1           1 1 2 * 1             1 * 1   1 * 1             
1 1 1               2 3 3 1           1 1 1   1 1 1             
1 1 1               1 * * 1                       1 1 1         
1 * 1               1 2 2 1                       1 * 1         
1 1 1                                             1 1 1         
                                1 1 1                           
                        1 1 1   1 * 1                           
                        1 * 1   1 1 2 1 1                       
                        1 1 1       1 * 1                   1 1 
                          1 1 1     1 1 1                   1 * 
                          1 * 1                             1 1 
                          1 1 1                   1 1 1         
    1 1 1                                         1 * 1         
    1 * 1                                         1 1 1         
    1 1 1                                                       
                      1 1 1                       1 1 1         
                      1 * 1                       1 * 1         
                      1 1 1         1 1 2 1 1     1 1 1         
                                    1 * 2 * 1                   
                                    2 2 3 1 1                   
                                    1 * 1                       
                          1 1 1     1 1 1                       
          1 1 1           2 * 2                 1 2 2 1         
      1 1 2 * 1           2 * 2                 1 * * 1         
      1 * 2 1 1           1 1 1                 1 2 2 1         

Oops, turns out medium is 16x16. Oh well, I can change that later on.
