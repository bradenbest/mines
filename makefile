all: mines mines-plain mines-debug
	cd src && mv $^ ../

%:
	cd src && $(MAKE) $@

clean:
	cd src && rm *.o

install: mines mines-plain
	mv $^ /usr/local/bin/ -i

uninstall:
	rm /usr/local/bin/mines -i
	rm /usr/local/bin/mines-plain -i

.PHONY: clean install uninstall
