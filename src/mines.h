typedef struct game game;

game *new_game( int width, int height, int number_of_bombs, int lives, int difficulty );

struct game{
    int width;
    int height;
    int num_bombs;
    int num_flipped;
    int num_flagged;
    int lives;
    int deaths;
    int sel_square;
    int difficulty;
    int *board;
};
