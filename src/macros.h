#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CHAR_BLANK         ' '
#define CHAR_BOMB          '*'
#define CHAR_BORDER_H      '+'
#define CHAR_BORDER_V      '+'
#define CHAR_FLAG          '!'
#define CHAR_SEL           '@'
#define CHAR_UNKNOWN       '.'
#define CHAR_FLAGGED_BOMB  '#'

#define CHAR_NUMBER(chr) \
    ('0' + chr)

#define COLOR(style, bg, fg) \
    "\e[" style ";4" bg ";3" fg "m"

#define COLOR_BG_BLACK   COLOR("1", "0", "7")
#define COLOR_BG_RED     COLOR("1", "1", "7")
#define COLOR_BG_GREEN   COLOR("0", "2", "0")
#define COLOR_BG_YELLOW  COLOR("0", "3", "0")
#define COLOR_BG_BLUE    COLOR("0", "4", "0")
#define COLOR_BG_PURPLE  COLOR("0", "5", "0")
#define COLOR_BG_CYAN    COLOR("0", "6", "0")
#define COLOR_BG_WHITE   COLOR("0", "7", "0")
#define COLOR_BG_BORDER  COLOR_BG_WHITE

#define COLOR_CLEAR_SCREEN "\e[2J"
#define COLOR_RESET        "\e[0;0;37m"

#define COMMAND_NULL       0
#define COMMAND_MOVE_UP    1
#define COMMAND_MOVE_RIGHT 2
#define COMMAND_MOVE_DOWN  3
#define COMMAND_MOVE_LEFT  4
#define COMMAND_FLIP       5
#define COMMAND_FLAG       6
#define COMMAND_QUIT       7

#define DIFFICULTY_S_EASY     16,     16,         5,     3, 0
#define DIFFICULTY_EASY        8,      8,        10,     1, 1
#define DIFFICULTY_MEDIUM     16,     16,        40,     1, 2
#define DIFFICULTY_HARD       30,     16,        99,     1, 3
#define DIFFICULTY_CUSTOM  width, height, num_bombs, lives, 4

#define DIR_INVALID   -1
#define DIR_UP         0
#define DIR_UP_RIGHT   1
#define DIR_RIGHT      2
#define DIR_DOWN_RIGHT 3
#define DIR_DOWN       4
#define DIR_DOWN_LEFT  5
#define DIR_LEFT       6
#define DIR_UP_LEFT    7

#define INVALID_DIR_UP    (out < 0)
#define INVALID_DIR_RIGHT (out % g->width == 0)
#define INVALID_DIR_DOWN  (out >= (g->width * g->height))
#define INVALID_DIR_LEFT  ( (out % g->width == g->width - 1) || (out < 0) )

#define MASK_BOMB (1<<0)
#define MASK_FLAG (1<<1)
#define MASK_FLIP (1<<2)

#define TERMINAL_UNKNOWN  0
#define TERMINAL_UNIX     1
#define TERMINAL_MAC      2
#define TERMINAL_WINDOWS  3

#ifndef TERMINAL
#  if defined __linux__ || defined __gnu_linux__ || defined __GNU__
#    define TERMINAL TERMINAL_UNIX
#  elif defined _WIN16 || defined _WIN32 || defined _WIN64 || defined __WIN32__
#    define TERMINAL TERMINAL_WINDOWS
#  elif defined __APPLE__ || defined __MACH__ || defined Macintosh
#    define TERMINAL TERMINAL_MAC
#  else
#    define TERMINAL TERMINAL_UNKNOWN
#  endif
#endif

//#undef TERMINAL
//#define TERMINAL TERMINAL_UNKNOWN
// Use to test plain-text UI

#define BLOCK(type, size) \
    (type *)malloc(sizeof(type) * size)
