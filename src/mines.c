/* mines.c
 *
 * Everything setup related goes here.
 *
 * Oh yeah, and since main() is here, that makes this the main file. 
 * So, ahem...
 *
 * This particular Mines clone (c) 2015 Braden Best
 */

#include <time.h>

#include "macros.h"
#include "mines.h"
#include "draw.h"
#include "input.h"
#include "game.h"

game *new_game(int width, int height, int number_of_bombs, int lives, int difficulty){
    game *g  = BLOCK(game, 1);
    int *board = BLOCK(int, width * height);
    int sel,
        num_bombs_temp = number_of_bombs;
    if (!g || !board){
        printf("Uh oh!\n");
        exit(1);
    }
    while(num_bombs_temp){
        sel = random_int(width * height); // select random square from board
        if(board[sel] & MASK_BOMB)
            continue;

        board[sel] |= MASK_BOMB;
        num_bombs_temp --;
    }

    g->width = width;
    g->height = height;
    g->num_bombs = number_of_bombs;
    g->num_flipped = 0;
    g->num_flagged = 0;
    g->lives = lives;
    g->deaths = 0;
    g->sel_square = 0;
    g->difficulty = difficulty;
    g->board = board;

    return g;
}

int main(int argc, char **argv){
    srand(time(0));
    int width,
        height,
        num_bombs,
        lives;
    game *g;
    if(argc > 1) {
        if (!strcmp(argv[1], "--s-easy")){
            g = new_game(DIFFICULTY_S_EASY);
        } else if (!strcmp(argv[1], "--easy")) {
            g = new_game(DIFFICULTY_EASY);
        } else if (!strcmp(argv[1], "--medium")) {
            g = new_game(DIFFICULTY_MEDIUM);
        } else if (!strcmp(argv[1], "--hard")) {
            g = new_game(DIFFICULTY_HARD);
        } else if (!strcmp(argv[1], "--custom")) {
            width     = get_input("Width: ");
            height    = get_input("Height: ");
            num_bombs = get_input("# of bombs: ");
            lives     = get_input("# of lives: ");
            g = new_game(DIFFICULTY_CUSTOM);
        } else {
            printf("Invalid option '%s'.\n", argv[1]);
            print_usage();
        }
    } else {
        print_usage();
    }
    game_loop(g);
    quit_game(g, 0);
    return 0;
}
