void game_loop( game *g );
int get_key();
int get_input( const char *prompt );
void parse_command( game *g, int command );
