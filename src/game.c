/* game.c
 *
 * Functions related to the logic and inner workings of the game.
 * Needless to say, most of the code is here.
 */

#include "macros.h"
#include "mines.h"
#include "draw.h"
#include "input.h"
#include "game.h"

void check_victory(game *g, int id){
    int status;
    enum{
        none,
        victory,
        failure
    };
    status = none;
    if (g->board[id] & MASK_BOMB)
        g->deaths ++;
        if(g->deaths == g->lives)
            status = failure;
    if (g->num_flipped == (g->width * g->height) - g->num_bombs)
        status = victory;

    if(status != none){
        flip_all_bombs(g);
        render(g);
        print_game_info(g);
        printf("\n\n\n\n\n"); // I don't like doing this, but I don't expect to be messing around with the controls, so...
        printf("You ");
        if (status == victory)
            printf("won!\n");
        else if (status == failure)
            printf("lost.\n");
        quit_game(g, 0);
    }
}

int count_adjacent(game *g, int id, int mask){
    int direction = 0,
        subsel,
        counter = 0;
    while(direction < 8){
        subsel = get_adjacent(g, id, direction);
        if (subsel != DIR_INVALID && g->board[subsel] & mask){
            counter++;
        }
        direction++;
    }
    return counter;
}

void flag_square(game *g, int id){
    if ( !(g->board[id] & MASK_FLIP) || ((g->board[id] & MASK_FLIP) && (g->board[id] & MASK_BOMB)) ) {
        // if not flipped, OR a flipped bomb (multiple lives allow this scenario)
        if(g->board[id] & MASK_FLAG) {
            g->num_flagged --;
        } else {
            g->num_flagged ++;
        }
        g->board[id] ^= MASK_FLAG;
    }
}

void flip_all_bombs(game *g){
    int i = 0;
    while(i < g->width * g->height){
        if(g->board[i] & MASK_BOMB){
            // only flip bombs
            g->board[i] |= MASK_FLIP;
        }
        i ++;
    }
}

void flip_square(game *g, int id){
    int num_bombs,
        num_flags,
        sel,
        direction = 0;
    if (!(g->board[id] & MASK_FLIP) && !(g->board[id] & MASK_FLAG)){
        /* If  square isn't flipped (prevent infinite recursion)
         * and square isn't flagged (prevent flipping a flagged bomb)
         * This section recursively flips non-number squares
         */
        g->board[id] |= MASK_FLIP;
        g->num_flipped ++;
        check_victory(g, id);
        num_bombs = count_adjacent(g, id, MASK_BOMB);
        if (num_bombs == 0){
            while(direction <= 7){
                sel = get_adjacent(g, id, direction);
                if (sel != DIR_INVALID){
                    flip_square(g, sel);
                }
                direction ++;
            }
        }
    } else if (g->board[id] & MASK_FLIP) {
        /* If square IS flipped
         * This special case flips the squares around a numbered square
         * This is a double-edged sword. If you flagged the wrong square, the
         * bomb WILL blow up and end your game
         */
        num_bombs = count_adjacent(g, id, MASK_BOMB);
        num_flags = count_adjacent(g, id, MASK_FLAG);
        if (num_flags == num_bombs && num_bombs > 0){
            while(direction <= 7){
                sel = get_adjacent(g, id, direction);
                if (sel != DIR_INVALID && !(g->board[sel] & MASK_FLIP)){
                    //If valid direction AND square isn't already flipped (again, infinite recursion)
                    flip_square(g, sel);
                }
                direction ++;
            }
        }
    }
}

int get_adjacent(game *g, int id, int direction){
    /* Returns ID of adjacent square in given direction
     * If direction is invalid for given square, returns -1
     */
    int out;
    switch(direction){
        case DIR_UP:
            out = id - g->width;
            if (INVALID_DIR_UP) out = DIR_INVALID;
            break;
        case DIR_RIGHT:
            out = id + 1;
            if (INVALID_DIR_RIGHT) out = DIR_INVALID;
            break;
        case DIR_DOWN:
            out = id + g->width;
            if (INVALID_DIR_DOWN) out = DIR_INVALID;
            break;
        case DIR_LEFT:
            out = id - 1;
            if (INVALID_DIR_LEFT) out = DIR_INVALID;
            break;
            /*
             * Conveniently enough, the diagonal logic is literally just
             * mixing the existing four-direction logic together.
             */
        case DIR_UP_RIGHT:
            out = (id - g->width) + 1;
            if (INVALID_DIR_UP || INVALID_DIR_RIGHT) out = DIR_INVALID;
            break;
        case DIR_DOWN_RIGHT:
            out = (id + g->width) + 1;
            if (INVALID_DIR_DOWN || INVALID_DIR_RIGHT) out = DIR_INVALID;
            break;
        case DIR_DOWN_LEFT:
            out = (id + g->width) - 1;
            if (INVALID_DIR_DOWN || INVALID_DIR_LEFT) out = DIR_INVALID;
            break;
        case DIR_UP_LEFT:
            out = (id - g->width) - 1;
            if (INVALID_DIR_UP || INVALID_DIR_LEFT) out = DIR_INVALID;
            break;
    }
    return out;
}

const char *get_difficulty(game *g){
    enum{
        s_easy = 0,
        easy   = 1,
        medium = 2,
        hard   = 3,
        custom = 4
    };
    switch(g->difficulty){
        case s_easy:
            return "Super Easy";
        case easy:
            return "Easy";
        case medium:
            return "Medium";
        case hard:
            return "Hard";
        case custom:
            return "Custom";
        default:
            return "Unknown (what!?)";
    }
}

int get_square(game *g, int x, int y){
    /* Returns ID of square at (x,y) */
    return x + (y * g->width);
}

int get_x_coord(game *g, int p){
    while(p > g->width)
        p -= g->width;
    return p % g->width;
}

int get_y_coord(game *g, int p){
    while(p % g->width)
        p --;
    return p / g->width;
}

void move_cursor(game *g, int direction){
    int out;
    switch(direction){
        case DIR_UP:
            out = g->sel_square - g->width;
            if (!INVALID_DIR_UP)
                g->sel_square = out;
            break;
        case DIR_RIGHT:
            out = g->sel_square + 1;
            if (!INVALID_DIR_RIGHT)
                g->sel_square = out;
            break;
        case DIR_DOWN:
            out = g->sel_square + g->width;
            if (!INVALID_DIR_DOWN)
                g->sel_square = out;
            break;
        case DIR_LEFT:
            out = g->sel_square - 1;
            if (!INVALID_DIR_LEFT)
                g->sel_square = out;
            break;
    }
}

void print_usage(){
    printf("Usage: mines [difficulty]\n");
    printf("Difficulties:\n");
    printf("    --s-easy    16x16,  5 bombs\n");
    printf("    --easy      8x8,   10 bombs\n");
    printf("    --medium    16x16, 40 bombs\n");
    printf("    --hard      30x16, 99 bombs\n");
    printf("    --custom    It's up to you.\n");
    exit(1);
}

void quit_game(game *g, int exit_code){
    free(g->board);
    free(g);
    exit(exit_code);
}

int random_int(int cap){
    return rand() % cap;
}
