/* draw.c
 *
 * Functions related to rendering the game.
 * This is where the TERMINAL macro becomes most prevalent.
 */

#include "macros.h"
#include "mines.h"
#include "draw.h"
#include "input.h"
#include "game.h"

void color_print(char c){
    switch(c){
        case '1':
            printf(COLOR_BG_BLACK "%c " COLOR_RESET, c);
            break;
        case '2':
            printf(COLOR_BG_BLUE "%c " COLOR_RESET, c);
            break;
        case '3':
            printf(COLOR_BG_YELLOW "%c " COLOR_RESET, c);
            break;
        case '4':
            printf(COLOR_BG_GREEN "%c " COLOR_RESET, c);
            break;
        case '5':
            printf(COLOR_BG_PURPLE "%c " COLOR_RESET, c);
            break;
        case '6':
            printf(COLOR_BG_RED "%c " COLOR_RESET, c);
            break;
        case '7':
            printf(COLOR_BG_CYAN "%c " COLOR_RESET, c);
            break;
        case '8':
            printf(COLOR_BG_WHITE "%c " COLOR_RESET, c);
            break;
        default:
            printf("%c ", c);
            break;
    }
}

void draw_border_bottom(game *g){
    if (TERMINAL == TERMINAL_UNIX){
        // Unix-specific. Draws simple white boxes
        printf(COLOR_BG_BORDER);
        draw_border_horiz(g->width + 2, ' ');
        printf(COLOR_RESET "\n");
    } else {
        printf("%c ", CHAR_BORDER_H);
        draw_border_horiz(g->width + 2, ' ');
        printf("%c\n", CHAR_BORDER_H);

        draw_border_horiz(g->width + 4, CHAR_BORDER_H);
        printf("\n");
    }
}

void draw_border_top(game *g){
    if (TERMINAL == TERMINAL_UNIX){
        // Unix-specific. Same as draw_border_bottom()
        draw_border_bottom(g);
    } else {
        draw_border_horiz(g->width + 4, CHAR_BORDER_H);
        printf("\n");

        printf("%c ", CHAR_BORDER_H);
        draw_border_horiz(g->width + 2, ' ');
        printf("%c\n", CHAR_BORDER_H);
    }
}

void draw_border_horiz(int width, char c){
    int i = 0;
    while(i++ < width)
        printf("%c ", c);
}

void print_game_info(game *g){
    int num_non_bombs;
    float percent[2];
    num_non_bombs = g->width * g->height - g->num_bombs;
    percent[0] = (float)g->num_flipped / (float)num_non_bombs;
    percent[1] = (float)g->num_flagged / (float)g->num_bombs;
    printf("Difficulty: %s (%ix%i, %i bombs)\n", get_difficulty(g), g->width, g->height, g->num_bombs);
    printf("Progress:\n");
    printf("  %i/%i squares flipped (%i%%)\n", g->num_flipped, num_non_bombs, (int)(percent[0] * 100) );
    printf("  %i/%i flags used (%i%%)\n",      g->num_flagged, g->num_bombs,  (int)(percent[1] * 100) );
    printf("  %i/%i lives remain\n",           g->lives - g->deaths, g->lives);
    printf("Position:   %i/%i (%i, %i)\n", g->sel_square, g->width * g->height - 1, get_x_coord(g, g->sel_square), get_y_coord(g, g->sel_square));
}

void render(game *g){
    int i = 0,
        j = 0,
        id;
    char c;
    if (TERMINAL == TERMINAL_UNIX){
        // Unix-specific. Slightly faster and more precise screen clearing
        printf(COLOR_CLEAR_SCREEN "\n");
    } else {
        while(i++ < 100){
            printf("\n");
        }
    }
    i = 0;
    draw_border_top(g);
    while(i < g->height){
        if (TERMINAL == TERMINAL_UNIX){
            // Unix-specific. Color. Left-side border
            printf(COLOR_BG_BORDER "  " COLOR_RESET);
        } else {
            printf("%c   ", CHAR_BORDER_V);
        }
        while(j < g->width){
            id = get_square(g, j, i);
            if ( !(g->board[id] & MASK_FLIP) ){
                c = (g->board[id] & MASK_FLAG) ? CHAR_FLAG : CHAR_UNKNOWN;
            } else {
                if (g->board[id] & MASK_BOMB){
                    if (g->board[id] & MASK_FLAG){
                        c = CHAR_FLAGGED_BOMB;
                    } else {
                        c = CHAR_BOMB;
                    }
                } else {
                    c = (char)count_adjacent(g, id, MASK_BOMB);
                    c = c ? CHAR_NUMBER(c) : CHAR_BLANK;
                }
            }
            if ( id == g->sel_square ){
                if (TERMINAL == TERMINAL_UNIX){
                    // Unix-specific. Prints the character with colors inverted.
                    printf(COLOR_BG_WHITE "%c " COLOR_RESET, c);
                } else {
                    printf("%c%c", CHAR_SEL, c);
                }
            } else {
                if (TERMINAL == TERMINAL_UNIX){
                    // Unix-specific. Prints the numbers with colors
                    color_print(c);
                } else {
                    printf("%c ", c);
                }
            }
            j++;
        }
        if (TERMINAL == TERMINAL_UNIX){
            // Unix-specific. Color. Right-side border
            printf(COLOR_BG_BORDER "  " COLOR_RESET "\n");
        } else {
            printf("  %c\n", CHAR_BORDER_V);
        }
        j = 0;
        i++;
    }
    draw_border_bottom(g);
}
