/* input.c
 *
 * Anything that deals with user input at runtime should be in here.
 */

#include "macros.h"
#include "mines.h"
#include "draw.h"
#include "input.h"
#include "game.h"

void game_loop(game *g){
    int command;
    while (1){
        render(g);
        print_game_info(g);
        command = get_key();
        parse_command(g, command);
    }
}

int get_key(){
    char cmd;
    printf("Controls:\n");
    printf("  h/j/k/l - move left/down/up/right\n");
    printf("  w/a/s/d - alternate movement controls\n");
    printf("  f/o     - flip selected square\n");
    printf("  g/p     - fla[g] selected square\n");
    printf("  q       - quit the game\n");

    if (TERMINAL == TERMINAL_UNIX){
        // Unix-specific. Enables input without pressing enter.
        system("/bin/stty raw");
        cmd = getchar();
        system("/bin/stty cooked");
    } else {
        cmd = getchar();
    }
    switch(cmd){
        case 'h':
        case 'a':
            return COMMAND_MOVE_LEFT;
        case 'j':
        case 's':
            return COMMAND_MOVE_DOWN;
        case 'k':
        case 'w':
            return COMMAND_MOVE_UP;
        case 'l':
        case 'd':
            return COMMAND_MOVE_RIGHT;
        case 'f':
        case 'o':
            return COMMAND_FLIP;
        case 'g':
        case 'p':
            return COMMAND_FLAG;
        case 'q':
            return COMMAND_QUIT;
        default:
            return COMMAND_NULL;
    }
}

int get_input(const char *prompt){
    char in[6];
    printf("%s", prompt);
    fgets(in, 6, stdin);
    switch(in[0]){
        case 'h':
            return DIR_LEFT;
        case 'j':
            return DIR_DOWN;
        case 'k':
            return DIR_UP;
        case 'l':
            return DIR_RIGHT;
    }
    return atoi(in);
}

void parse_command(game *g, int command){
    switch(command){
        case COMMAND_NULL:
            break;
        case COMMAND_MOVE_UP:
            move_cursor(g, DIR_UP);
            break;
        case COMMAND_MOVE_RIGHT:
            move_cursor(g, DIR_RIGHT);
            break;
        case COMMAND_MOVE_DOWN:
            move_cursor(g, DIR_DOWN);
            break;
        case COMMAND_MOVE_LEFT:
            move_cursor(g, DIR_LEFT);
            break;
        case COMMAND_FLIP:
            flip_square(g, g->sel_square);
            break;
        case COMMAND_FLAG:
            flag_square(g, g->sel_square);
            break;
        case COMMAND_QUIT:
            quit_game(g, 0);
            break;
    }
}
