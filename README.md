# Mines

I made a mines game!

It went very smoothly.

![Demo 1](./demo1.gif)
![Demo 2](./demo2.gif)

## Building

To compile, simply use `make`

    $ make mines

or if you want the debug and plain-text versions

    $ make mines-plain
    $ make mines-debug

`make` by itself or `make all` will build all three.

## Installing

Also simple. You can compile and install both `mines` and `mines-plain` with:

    $ sudo make install

To uninstall, you guessed it:

    $ sudo make uninstall

## Usage

    $ mines       # For the full game
    $ mines-plain # For a limited version of the game
    
If `mines` doesn't look right (Do you see `^[0;47;30m` all over the place?), then you'll want to run `mines-plain` so you don't go bonkers looking at the game.

### mines-plain

It's the same game, but with certain limitations. Mainly:

1. Input is line-buffered, so you have to hit enter to send a key.
2. Color escape sequences are gone, so the UI is a little different.

This way, it should work in, for example, Windows' cmd.exe prompt.

### mines-debug

This is for debugging purposes. As in...

    $ gdb mines-debug
    (gdb) set args --hard
    (gdb) break flip_all
    (gdb) run

You can run mines-debug by itself, but since it's optimized for use with gdb, what would be the point of that? It's nearly twice the size of regular `mines`!
